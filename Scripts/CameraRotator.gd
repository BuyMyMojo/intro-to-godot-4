extends Node3D

@export var roation_speed: float = 3.5

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var final_rotation_speed: float = roation_speed * delta
	
	rotation.y += final_rotation_speed
	
	pass
