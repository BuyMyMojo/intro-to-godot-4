extends Area2D

var camera

func _ready():
	camera = get_viewport().get_camera_2d()

func _on_body_entered(body:Node2D):
	body.scale += Vector2(0.2, 0.2)

	if camera.zoom.x >= 0.3:
		camera.zoom -= Vector2(0.02, 0.02)

	queue_free()
